package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SecondExampleApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context =  SpringApplication.run(SecondExampleApplication.class, args);

    }

}