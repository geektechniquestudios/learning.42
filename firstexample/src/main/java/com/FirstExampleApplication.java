package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class FirstExampleApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context =  SpringApplication.run(FirstExampleApplication.class, args);

        Monster monster = context.getBean(Monster.class);
        monster.doSomething();

        Monster monster2 = context.getBean(Monster.class);
        monster2.doSomething();

    }

}