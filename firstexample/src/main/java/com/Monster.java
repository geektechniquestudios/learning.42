package com;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
//@Scope("prototype")
public class Monster {

    private int id;
    private String name;
    private String tech;

    @Autowired
    @Qualifier("shoes1") //name of bean
    private Shoes shoes;

    public Monster() {
        System.out.println("Monster Created");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAname() {
        return name;
    }

    public void setAname(String aname) {
        this.name = aname;
    }

    public String getTech() {
        return tech;
    }

    public void setTech(String tech) {
        this.tech = tech;
    }

    public void doSomething(){
        System.out.println("print from monster");
        shoes.doSomethingelse();
    }
}
