package com;

import org.springframework.stereotype.Component;

@Component("shoes1")
public class Shoes {
    private int id;
    private String brand;

    public void doSomethingelse(){
        System.out.println("hello from something shoes");
    }

    public Shoes() {
        System.out.println("shoes created");
    }

    @Override
    public String toString() {
        return "Shoes{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
